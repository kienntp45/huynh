﻿using EF_Core_OracleSQL.Application.Commnad.CreateStudentCommand;
using EF_Core_OracleSQL.Data;
using EF_Core_OracleSQL.Data.entities;
using EF_Core_OracleSQL.Service.IService;
using Microsoft.EntityFrameworkCore;

namespace EF_Core_OracleSQL.Service
{
    public class StudentService : IStudentService
    {
        private readonly MyDbContext _myContext;

        public StudentService(MyDbContext Context) 
        => _myContext = Context;

        public async Task<Student> CreateStudent(InsertStudentCommand CreateStudentCommand)
        {
            var Student = new Student
            {
                Name = CreateStudentCommand.Name,
                Birthday = CreateStudentCommand.Birthday,
                Gender = CreateStudentCommand.Gender,
                Status = CreateStudentCommand.Status,
            };
            _myContext.Students.Add(Student);
            await _myContext.SaveChangesAsync();
            return Student;
        }

        public async Task<bool> DeleteStudent(int StudentId)
        {
            var studentId = await _myContext.Students.FirstOrDefaultAsync(s => s.Id == StudentId);
            if (studentId != null) 
            {
                _myContext.Students.Remove(studentId);
                await _myContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<List<Student>> GetAllStudent()
        => await _myContext.Students.ToListAsync();

        public async Task<Student> GetStudentById(int studentId)
        => await _myContext.Students.Include(s => s.Marks).FirstOrDefaultAsync(s => s.Id == studentId);
        

        public async Task<Student> UpdateStudent(int StudentId, UpdateStudentCommand UpdateStudentCommand)
        {
            var student = await _myContext.Students.FirstOrDefaultAsync(s => s.Id == StudentId);
            if (student != null)
            {
                student.Name = UpdateStudentCommand.StudentView.Name;
                student.Birthday = UpdateStudentCommand.StudentView.Birthday;
                student.Gender = UpdateStudentCommand.StudentView.Gender;
                student.Status = UpdateStudentCommand.StudentView.Status;

                await _myContext.SaveChangesAsync();
                return student;
            }
            return null;
        }
    }
}
