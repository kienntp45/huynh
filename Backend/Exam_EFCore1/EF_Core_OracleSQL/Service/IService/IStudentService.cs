﻿namespace EF_Core_OracleSQL.Service.IService;
public interface IStudentService
{
    Task<List<Student>> GetAllStudent();
    Task<Student> CreateStudent(InsertStudentCommand CreateStudentCommand);
    Task<Student> UpdateStudent(int StudentId, UpdateStudentCommand UpdateStudentCommand);
    Task<bool> DeleteStudent(int StudentId);
    Task<Student> GetStudentById(int studentId);
}
