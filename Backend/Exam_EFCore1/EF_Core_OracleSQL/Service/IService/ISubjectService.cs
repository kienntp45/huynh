﻿namespace EF_Core_OracleSQL.Service.IService
{
    public interface ISubjectService
    {
        Task<List<Subject>> GetAllSubject();
        Task<Subject>CreateSubject (InsertSubjectCommand subjectCommand);
        Task<Subject> UpdateSubject (int id , UpdateSubjectCommand updateSubjectCommand);
        Task<bool> DeleteSubject(int id);
        Task<Subject> GetSubjectWithMarksAndStudentsById(int subjectId);
    }
}
