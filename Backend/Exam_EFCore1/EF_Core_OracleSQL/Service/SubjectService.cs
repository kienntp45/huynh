﻿namespace EF_Core_OracleSQL.Service;

public class SubjectService : ISubjectService
{
    private readonly MyDbContext _dbContext;

    public SubjectService(MyDbContext DbContext)
    => _dbContext = DbContext;

    public async Task<Subject> CreateSubject(InsertSubjectCommand SubjectView)
    {
        var subject = new Subject
        {
            Name = SubjectView.Name,
            Status = SubjectView.Status,
        };
        _dbContext.Subjects.Add(subject);
        await _dbContext.SaveChangesAsync();
        return subject;
    }

    public async Task<bool> DeleteSubject(int id)
    {
       var SubjectId = await _dbContext.Subjects.SingleOrDefaultAsync(x => x.Id == id);
        if(SubjectId != null) {
            _dbContext.Subjects.Remove(SubjectId);
            await _dbContext.SaveChangesAsync();
            return true;
        }
        return false;
    }

    public async Task<List<Subject>> GetAllSubject()
    => await _dbContext.Subjects.ToListAsync();

    public async Task<Subject> GetSubjectWithMarksAndStudentsById(int subjectId)
    => await _dbContext.Subjects.Where(s => s.Id == subjectId).Include(s => s.Marks).ThenInclude(m => m.Student).FirstOrDefaultAsync();
    

    public async Task<Subject> UpdateSubject(int id, UpdateSubjectCommand updateSubjectCommand)
    {
       var SubjectId = await _dbContext.Subjects.FirstOrDefaultAsync( s => s.Id==id );
        if(SubjectId != null)
        {
            SubjectId.Name = updateSubjectCommand.SubjectView.Name;
            SubjectId.Status = updateSubjectCommand.SubjectView.Status;

            await _dbContext.SaveChangesAsync();
            return SubjectId;
        }
        return null;
    }

} 
