﻿namespace EF_Core_OracleSQL.Controllers;

[Route("api/[controller]")]
[ApiController]
public class StudentController : ControllerBase
{
    private readonly IStudentService _studentService;

    public StudentController(IStudentService studentService) 
      =>  _studentService = studentService;
    
    [HttpGet]
    public async Task<IActionResult> GetAllStudent()
    {
        try
        {
            var GetAllStudent = await _studentService.GetAllStudent();
            return Ok(GetAllStudent);
        }
        catch (Exception)
        {

            throw;
        }
    }

    [HttpPost]
    public async Task<IActionResult> CreateStudent(InsertStudentCommand StudentView)
    {
        var CreateStudent = await _studentService.CreateStudent(StudentView);
        if(CreateStudent != null)
        {
            return Ok(BaseRepon.Succress(SuccressContent.CodeSuccress,SuccressContent.MessageSuccress,CreateStudent));
        }
        return BadRequest(BaseRepon.Succress(SuccressContent.CodeFail, SuccressContent.MessageFail, CreateStudent));
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateStudent(int id, UpdateStudentCommand updateCommand)
    {
        updateCommand.StudentId = id;
        var updateStudent = await _studentService.UpdateStudent(id, updateCommand);
        if (updateStudent != null) ;
        return Ok(BaseRepon.Succress(SuccressContent.CodeSuccress, SuccressContent.MessageSuccress, updateStudent));
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteStudent(int id)
    => Ok(BaseRepon.Succress(SuccressContent.CodeSuccress, SuccressContent.MessageSuccress,_studentService.DeleteStudent(id)));
      
    [HttpGet("{id}")]
    public async Task<IActionResult> GetStudentById(int id)
    {
        var student = await _studentService.GetStudentById(id);
        if (student != null)
        {
            return Ok(student);
        }
        return NotFound();
    }

}
