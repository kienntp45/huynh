﻿namespace EF_Core_OracleSQL.Controllers;

[Route("api/[controller]")]
[ApiController]
public class SubjectController : ControllerBase
{
    private readonly ISubjectService _subjectService;

    public SubjectController(ISubjectService subjectService) 
    =>_subjectService = subjectService;

    [HttpGet]
    public async Task<IActionResult> GetAllSubject()
    {
        try
        {
            var GetAllSubject = _subjectService.GetAllSubject();
            return Ok(GetAllSubject);
        }
        catch (Exception)
        {
            throw;
        }
    }

    [HttpPost]
    public async Task<IActionResult> CreateSubject (InsertSubjectCommand InsertSubjectCommand)
    {
        var CreateSubject = _subjectService.CreateSubject(InsertSubjectCommand);
       return Ok(BaseRepon.Succress(SuccressContent.CodeSuccress, SuccressContent.MessageSuccress, CreateSubject));
       
    }
    
    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateSubjects(int id ,UpdateSubjectCommand UpdateSubjectCommand)
    {
       UpdateSubjectCommand.SubjectID = id;
       var subjectId = await _subjectService.UpdateSubject(id,UpdateSubjectCommand);
        if(subjectId != null)
        {
            return Ok(BaseRepon.Succress(SuccressContent.CodeSuccress,SuccressContent.MessageSuccress,subjectId));
        }
        return BadRequest(BaseRepon.Succress(SuccressContent.CodeFail, SuccressContent.MessageFail, subjectId));
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteSbjectId(int id)
    => Ok(BaseRepon.Succress(SuccressContent.CodeSuccress, SuccressContent.MessageSuccress,_subjectService.DeleteSubject(id)));

    [HttpGet("{id}")]
    public async Task<IActionResult> GetSubjectById(int id)
    {
        var subject = await _subjectService.GetSubjectWithMarksAndStudentsById(id);
        if (subject != null)
        {
            return Ok(subject);
        }
        return NotFound();
    }

}
