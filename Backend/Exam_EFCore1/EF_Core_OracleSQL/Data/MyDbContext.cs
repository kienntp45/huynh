﻿namespace EF_Core_OracleSQL.Data;

public class MyDbContext :DbContext
{
    public MyDbContext(DbContextOptions options) : base(options)
    { }

    #region DbSet
    public DbSet<Student>  Students { get; set; }
    public DbSet<Mark> Marks { get; set; }
    public DbSet<Subject> Subjects { get; set; }
    #endregion
}
