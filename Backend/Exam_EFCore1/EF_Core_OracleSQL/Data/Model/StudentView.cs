﻿namespace EF_Core_OracleSQL.Data.Model;

public class StudentView
{
    public string Name { get; set; }
    public DateTime Birthday { get; set; }
    public int Gender { get; set; }
    public bool Status { get; set; }
}
