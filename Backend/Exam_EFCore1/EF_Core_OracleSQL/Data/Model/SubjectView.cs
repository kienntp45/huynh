﻿namespace EF_Core_OracleSQL.Data.Model;

public class SubjectView
{
    public string Name { get; set; }
    public bool Status { get; set; }
}
