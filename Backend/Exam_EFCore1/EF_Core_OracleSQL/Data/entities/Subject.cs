﻿namespace EF_Core_OracleSQL.Data.entities;

[Table("Subject")]
public class Subject
{
    [Key]
    [Column("SubjectId")]
    public int Id { get; set; }

    [Required]
    [Column("SubjectName")]
    [StringLength(200)]
    public string Name { get; set; }

    [Required]
    [Column("Status")]
    public bool Status { get; set; }
    // Thêm navigation property để ánh xạ mối quan hệ một-nhiều với bảng Mark
    public ICollection<Mark> Marks { get; set; }
}
