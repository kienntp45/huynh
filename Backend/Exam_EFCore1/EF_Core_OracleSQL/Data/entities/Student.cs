﻿namespace EF_Core_OracleSQL.Data.entities;

[Table("Student")]
public class Student
{
    [Key]
    public int Id { get; set; }

    [Required]
    [MaxLength(100)]
    public string Name { get; set; }

    public DateTime Birthday { get; set; }

    public int Gender { get; set; }

    public bool Status { get; set; }
    // Thêm navigation property để ánh xạ mối quan hệ một-nhiều với bảng Mark
    public ICollection<Mark> Marks { get; set; }
}
