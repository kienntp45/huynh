﻿namespace EF_Core_OracleSQL.Data.entities;

[Table("Mark")]
public class Mark
{
    [Key]
    public int Id { get; set; } // Id của Mark

    [Required]
    public int StudentId { get; set; }

    [Required]
    public int SubjectId { get; set; }

    [Required]
    public int Scores { get; set; }

    [Required]
    [Column("CreateDate")]
    public DateTime CreateDate { get; set; }

    // Navigation property đến Student
    public Student Student { get; set; }

    // Navigation property đến Subject
    public Subject Subject { get; set; }
}
