﻿global using System.ComponentModel.DataAnnotations.Schema;
global using System.ComponentModel.DataAnnotations;
global using EF_Core_OracleSQL.Data.entities;
global using MediatR;
global using EF_Core_OracleSQL.Data.Model;

global using EF_Core_OracleSQL.Application.Commnad;
global using EF_Core_OracleSQL.Application.Commnad.CreateStudentCommand;
global using EF_Core_OracleSQL.Common;
global using EF_Core_OracleSQL.Service.IService;
global using Microsoft.AspNetCore.Mvc;
global using EF_Core_OracleSQL.Application.Commnad.CreateSubjectCommand;
global using Microsoft.EntityFrameworkCore;

global using EF_Core_OracleSQL.Data;