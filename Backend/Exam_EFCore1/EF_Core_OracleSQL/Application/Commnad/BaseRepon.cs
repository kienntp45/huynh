﻿namespace EF_Core_OracleSQL.Application.Commnad;

public class BaseRepon
{
    public int Code {  get; set; }
    public string Messages { get; set; }
    public object Data { get; set; }

    public static BaseRepon Succress (int code, string messages , object data = null )
    => new BaseRepon { Code = code, Messages = messages, Data = data };
}
