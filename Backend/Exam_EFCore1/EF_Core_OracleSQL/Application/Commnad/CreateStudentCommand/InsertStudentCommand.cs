﻿namespace EF_Core_OracleSQL.Application.Commnad.CreateStudentCommand;

public class InsertStudentCommand : IRequest<Student> 
{
    public string Name { get; set; }
    public DateTime Birthday { get; set; }
    public int Gender { get; set; }
    public bool Status { get; set; }
}
