﻿namespace EF_Core_OracleSQL.Application.Commnad.CreateStudentCommand;

public class UpdateStudentCommand :IRequest<Student>
{
    public int StudentId {  get; set; }
    public StudentView StudentView { get; set; }
}
