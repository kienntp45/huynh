﻿namespace EF_Core_OracleSQL.Application.Commnad.CreateSubjectCommand;

public class InsertSubjectCommand : IRequest<Subject>
{
    public string Name { get; set; }
    public bool Status { get; set; }
}
