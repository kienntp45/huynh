﻿namespace EF_Core_OracleSQL.Application.Commnad.CreateSubjectCommand;

public class UpdateSubjectCommand : IRequest<Subject>
{
    public int SubjectID { get; set; }
    public SubjectView SubjectView { get; set; }
}
