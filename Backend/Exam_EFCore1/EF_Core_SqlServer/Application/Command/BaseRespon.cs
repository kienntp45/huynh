﻿namespace EF_Core_SqlServer.Application.Command;

public class BaseRespon
{
    public int Code { get; set; }
    public string Message { get; set; }
    public object Data { get; set; }

    public static BaseRespon CreateSuccess(int code, string message, object data = null)
         => new BaseRespon { Code = code, Message = message, Data = data };

    public static BaseRespon CreateFailure(string message, object data = null)
      => new BaseRespon { Code = SuccressContent.CodeFail, Message = message, Data = data
};
}
