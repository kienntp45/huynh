﻿namespace EF_Core_SqlServer.Application.Command.InsertSubject
{
    public class CreateSubjectCommad : IRequest<Subject>
    {
        public string Name { get; set; }
        public bool Status { get; set; }
    }
}
