﻿namespace EF_Core_SqlServer.Application.Command.InsertSubject
{
    public class UpdateSubjectCommand :IRequest<Subject>
    {
        public int SubjectID { get; set; }
        public SubjectView UpdateSubject { get; set; }
    }
}
