﻿namespace EF_Core_SqlServer.Application.Command.InsertSubject
{
    public class DeleteSubjectCommand :IRequest<bool>
    {
        public int subjectID { get; set; }
    }
}
