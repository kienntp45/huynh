﻿namespace EF_Core_SqlServer.Application.Command.InsertStudent
{
    public class UpdateStudentCommand :IRequest<Student>
    {
        public int StudentId { get; set; }
        public StudentView updateStudent { get; set; }
    }
}
