﻿namespace EF_Core_SqlServer.Application.Command.InsertStudent
{
    public class GetStudentWithMark :IRequest<StudentWithMarksViewModel>
    {
        public int StudentId { get; set; }
    }
}
