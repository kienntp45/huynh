﻿namespace EF_Core_SqlServer.Application.Command.InsertStudent
{
    public class DeleteStudentCommand : IRequest<bool>
    {
        public int StudentId { get; set; }
    }
}
