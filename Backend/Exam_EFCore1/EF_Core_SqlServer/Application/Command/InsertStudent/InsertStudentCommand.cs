﻿namespace EF_Core_SqlServer.Application.Command.InsertStudent
{
    public class InsertStudentCommand : IRequest<Student>
    {
        public string Name { get; set; }
        public DateTime Birthday { get; set; }
        public int Gender { get; set; }
        public bool Status { get; set; }
    }
}
