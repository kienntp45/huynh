﻿global using EF_Core_SqlServer.Data.Model;
global using MediatR;

//controler
global using EF_Core_SqlServer.service;

global using EF_Core_SqlServer.Application.Command;
global using EF_Core_SqlServer.Application.Command.InsertStudent;
global using EF_Core_SqlServer.common;
global using EF_Core_SqlServer.service.IService;
global using Microsoft.AspNetCore.Mvc;
global using EF_Core_SqlServer.Application.Command.InsertSubject;


// entities
global using System.ComponentModel.DataAnnotations;
global using System.ComponentModel.DataAnnotations.Schema;
global using EF_Core_SqlServer.Service.IService;


global using Microsoft.EntityFrameworkCore;
global using EF_Core_SqlServer.Data;