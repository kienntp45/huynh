﻿namespace EF_Core_SqlServer.service;

public class StudentServices : IStudentService
{
    private readonly MyDBContextStudent _dbContext;

    public StudentServices(MyDBContextStudent context)
    {
        _dbContext = context;
    }

    public async Task<List<Student>> GetAllStudent()
    => await _dbContext.Students.ToListAsync();
    

    public async Task<bool> DeleteStudent(int id)
    {
        var student = await _dbContext.Students.FirstOrDefaultAsync(s => s.Id == id);

        if (student != null)
        {
            _dbContext.Students.Remove(student);
            await _dbContext.SaveChangesAsync();
            return true;
        }

        return false;
    }

    public async Task<Student> CreateStudent(InsertStudentCommand command)
    {
        var student = new Student
        {
            Name = command.Name,
            Birthday = command.Birthday,
            Gender = command.Gender,
            Status = command.Status
        };

        _dbContext.Students.Add(student);
        await _dbContext.SaveChangesAsync();

        return student;
    }

    public async Task<Student> UpdateStudent(int id, UpdateStudentCommand studentcommand)
    {
        var student = await _dbContext.Students.FirstOrDefaultAsync(s => s.Id == id);

        if (student != null)
        {
            student.Name = studentcommand.updateStudent.Name;
            student.Birthday = studentcommand.updateStudent.Birthday;
            student.Gender = studentcommand.updateStudent.Gender;
            student.Status = studentcommand.updateStudent.Status;

            await _dbContext.SaveChangesAsync();

            return student;
        }

        return null;
    }

    public async Task<StudentWithMarksViewModel> Handle(GetStudentWithMark request, CancellationToken cancellationToken)
    {
        var student = await _dbContext.Students
            .Include(s => s.Marks)
            .Where(s => s.Id == request.StudentId)
            .Select(s => new StudentWithMarksViewModel
            {
                StudentId = s.Id,
                StudentName = s.Name,
                Marks = s.Marks.ToList()
            })
            .FirstOrDefaultAsync();

        return student;
    }
}
