﻿namespace EF_Core_SqlServer.service;

public class SubjectServices : ISubjectService
{
    private readonly MyDBContextStudent _dbContext;

    public SubjectServices(MyDBContextStudent DBcontext) 
    {
        _dbContext = DBcontext;
    }

    public async Task<Subject> CreateSubject(CreateSubjectCommad Subjectcommand)
    {
        var subject = new Subject
        {
            Name = Subjectcommand.Name,
            Status = Subjectcommand.Status
        };
        _dbContext.Subjects.Add(subject);
        await _dbContext.SaveChangesAsync(); 
        return subject;
    }

    public async Task<List<Subject>> GetAll()
    {
        var listds = _dbContext.Subjects.ToList();
        return listds;
    }

    public async Task<bool> DeleteSubject(int subjectId)
    {
        var existingSubject = await _dbContext.Subjects.FindAsync(subjectId);

        if (existingSubject == null)
        {
            return false;
        }

        _dbContext.Subjects.Remove(existingSubject);
        await _dbContext.SaveChangesAsync();

        return true;
    }

    public async Task<Subject> UpdateSubject(int subjectId, UpdateSubjectCommand updatedSubject)
    {
        var UpdateSubject = await _dbContext.Subjects.FindAsync(subjectId);
        if(UpdateSubject == null)
        {
            return null;
        }
        UpdateSubject.Name = updatedSubject.UpdateSubject.Name;
        UpdateSubject.Status = updatedSubject.UpdateSubject.Status;

        _dbContext.Subjects.Update(UpdateSubject);
        await _dbContext.SaveChangesAsync();

        return UpdateSubject;
    }

    public async Task<SubjectWithMarksAndStudentsViewModel> GetSubjectWithMarksAndStudents(int subjectId)
    {
        var subjectData = await _dbContext.Subjects
            .Where(s => s.Id == subjectId)
            .Select(s => new SubjectWithMarksAndStudentsViewModel
            {
                SubjectId = s.Id,
                SubjectName = s.Name,
                Marks = s.Marks
                    .Select(m => new MarkWithStudentViewModel
                    {
                        MarkId = m.Id,
                        Scores = m.Scores,
                        Student = new StudentViewModel
                        {
                            StudentId = m.Student.Id,
                            StudentName = m.Student.Name
                        }
                    })
                    .ToList()
            })
            .FirstOrDefaultAsync();

        return subjectData;
    }
}
