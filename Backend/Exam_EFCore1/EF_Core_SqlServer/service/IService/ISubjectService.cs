﻿namespace EF_Core_SqlServer.Service.IService;

public interface ISubjectService
{
    Task<List<Subject>> GetAll();
    Task<Subject> CreateSubject(CreateSubjectCommad subjectCommand);
    Task<bool> DeleteSubject(int subjectId);
    Task<Subject> UpdateSubject(int subjectId, UpdateSubjectCommand updatedSubject);
    Task<SubjectWithMarksAndStudentsViewModel> GetSubjectWithMarksAndStudents(int subjectId);
}
