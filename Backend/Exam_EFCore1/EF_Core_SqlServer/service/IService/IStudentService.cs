﻿namespace EF_Core_SqlServer.service.IService;

public interface IStudentService
{
    Task<List<Student>> GetAllStudent();
    Task<Student> CreateStudent(InsertStudentCommand command);
    Task<Student> UpdateStudent(int id, UpdateStudentCommand studentcommand);
    Task<bool> DeleteStudent(int id);
    Task<StudentWithMarksViewModel> Handle(GetStudentWithMark request, CancellationToken cancellationToken);

}
