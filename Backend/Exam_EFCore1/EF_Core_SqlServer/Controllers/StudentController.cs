﻿namespace EF_Core_SqlServer.Controllers;

[Route("api/[controller]")]
[ApiController]
public class StudentController : ControllerBase
{
    private readonly IStudentService _studentService;

    public StudentController(IStudentService studentService)
    => _studentService = studentService;

    [HttpGet]
    public async Task<IActionResult> GetAll()
    {
        var students = await _studentService.GetAllStudent();
        if (students != null && students.Count > 0)
        {
            return Ok(students);
        }
        return BadRequest("Danh sách trống");
    }

    [HttpPost]
    public async Task<IActionResult> CreateStudent(InsertStudentCommand studentView)
    {
        var createdStudent = await _studentService.CreateStudent(studentView);

        if (createdStudent != null)
        {
            return Ok(BaseRespon.CreateSuccess(SuccressContent.CodeSuccress, SuccressContent.MessageSuccress, createdStudent));
        }
        return BadRequest(BaseRespon.CreateFailure("Không thể tạo học sinh"));
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateStudent(int id, UpdateStudentCommand command)
    {
        command.StudentId = id;
        var updatedStudent = await _studentService.UpdateStudent(id, command);

        if (updatedStudent != null)
        {
            return Ok(BaseRespon.CreateSuccess(SuccressContent.CodeSuccress, SuccressContent.MessageSuccress, updatedStudent));
        }
        return BadRequest(BaseRespon.CreateFailure("Không tìm thấy học sinh hoặc không thể cập nhật."));
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteStudent(int id)
    {
        var student = await _studentService.DeleteStudent(id);
        if (student)
        {
            return Ok(BaseRespon.CreateSuccess(SuccressContent.CodeSuccress, SuccressContent.MessageSuccress));
        }
        return Ok(BaseRespon.CreateFailure(SuccressContent.MessageFail));
    }

    [HttpGet("student/{id}")]
    public async Task<IActionResult> GetStudentWithMarks(int id)
    {
        var query = new GetStudentWithMark { StudentId = id };
        var studentWithMarks = await _studentService.Handle(query, CancellationToken.None);

        return Ok(BaseRespon.CreateSuccess(SuccressContent.CodeSuccress, SuccressContent.MessageSuccress));
    }
}
