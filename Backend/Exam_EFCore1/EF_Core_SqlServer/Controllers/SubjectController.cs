﻿namespace EF_Core_SqlServer.Controllers;

[Route("api/[controller]")]
[ApiController]
public class SubjectController : ControllerBase
{
    private readonly ISubjectService _subjectService;

    public SubjectController(ISubjectService subjectService)    
       => _subjectService = subjectService;

    [HttpGet]
    public async Task<IActionResult> GetAll()
    {
        var getall = await _subjectService.GetAll();
        if (getall != null)
        {
            return Ok(getall);
        }
        return BadRequest("Danh sách trống");
    }

    [HttpPost]
    public async Task<IActionResult> CreateSubject(CreateSubjectCommad subjectView)
    {
        var createdSubject = await _subjectService.CreateSubject(subjectView);
        if (createdSubject != null)
        {
            return Ok(BaseRespon.CreateSuccess(SuccressContent.CodeSuccress, SuccressContent.MessageSuccress, createdSubject));
        }
            return Ok(BaseRespon.CreateFailure(SuccressContent.MessageFail));
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteSubject(int id)
    {
            var result = await _subjectService.DeleteSubject(id);
            if (result)
            {
                return Ok(BaseRespon.CreateSuccess(SuccressContent.CodeSuccress, SuccressContent.MessageSuccress));
            }
            else
            {
                return Ok(BaseRespon.CreateFailure(SuccressContent.MessageSuccress));
            }
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateSubject(int id, UpdateSubjectCommand updatedSubject)
    {
            var result = await _subjectService.UpdateSubject(id, updatedSubject);
            if (result != null)
            {
            return Ok(BaseRespon.CreateSuccess(SuccressContent.CodeSuccress, SuccressContent.MessageSuccress));
        }
            else
            {
            return Ok(BaseRespon.CreateFailure(SuccressContent.MessageSuccress));
        }
    }

    [HttpGet("subject/{id}")]
    public async Task<IActionResult> GetSubjectWithMarksAndStudents(int id)
    {
        var subjectData = await _subjectService.GetSubjectWithMarksAndStudents(id);
        return Ok(subjectData);
    }
}
