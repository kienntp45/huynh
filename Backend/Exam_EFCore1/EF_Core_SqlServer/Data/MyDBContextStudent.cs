﻿namespace EF_Core_SqlServer.Data;

public class MyDBContextStudent : DbContext
{
    public MyDBContextStudent(DbContextOptions options) : base(options)
    { }
        // DBSet cho các model
        #region Dbset
           public DbSet<Student> Students { get; set; } //Student
           public DbSet<Mark> Marks { get; set; } // mark
           public DbSet<Subject> Subjects { get; set; } // Subject

    #endregion
}

