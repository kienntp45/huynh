﻿namespace EF_Core_SqlServer.Data.Model;

[Table("Mark")]
public class Mark
{
    [Key]
    public int Id { get; set; } // Id của Mark

    [ForeignKey("Student")]
    [Column("StudentId")]
    [Required]
    public int StudentId { get; set; }

    [ForeignKey("Subject")]
    [Column("SubjectId")]
    [Required]
    public int SubjectId { get; set; }

    [Required]
    public int Scores { get; set; }

    [Required]
    [Column("CreateDate")]
    public DateTime CreateDate { get; set; }

    public Student Student { get; set; } // Navigation property đến Student

    public Subject Subject { get; set; } // Navigation property đến Subject
}
