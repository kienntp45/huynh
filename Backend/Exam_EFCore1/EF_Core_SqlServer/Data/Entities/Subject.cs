﻿namespace EF_Core_SqlServer.Data.Model;

[Table("subject")]
public class Subject
{
    [Key]
    [Column("SubjectId")]
    public int Id { get; set; }

    [Required]
    [Column("SubjectName")]
    [StringLength(200)]
    public string Name { get; set; }

    [Required]
    [Column("Status")]
    public bool Status { get; set; }

    public ICollection<Mark> Marks { get; set; }
}
