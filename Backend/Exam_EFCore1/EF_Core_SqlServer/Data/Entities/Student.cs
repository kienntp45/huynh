﻿namespace EF_Core_SqlServer.Data.Model;

[Table("Student")]
public class Student
{
    [Key]
    public int Id { get; set; }

    [Required]
    [MaxLength(100)]
    public string Name { get; set; }

    public DateTime Birthday { get; set; }

    public int Gender { get; set; }

    public bool Status { get; set; }

    public ICollection<Mark> Marks { get; set; }
}
