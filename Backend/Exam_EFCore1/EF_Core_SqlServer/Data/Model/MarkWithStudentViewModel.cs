﻿namespace EF_Core_SqlServer.Data.Model;

public class MarkWithStudentViewModel
{
    public int MarkId { get; set; }
    public int Scores { get; set; }
    public StudentViewModel Student { get; set; }
}
