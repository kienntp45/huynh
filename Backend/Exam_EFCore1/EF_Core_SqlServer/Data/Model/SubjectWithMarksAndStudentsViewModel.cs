﻿namespace EF_Core_SqlServer.Data.Model;

public class SubjectWithMarksAndStudentsViewModel
{
    public int SubjectId { get; set; }
    public string SubjectName { get; set; }
    public List<MarkWithStudentViewModel> Marks { get; set; }
}
