﻿namespace EF_Core_SqlServer.Data.Model;

public class StudentWithMarksViewModel
{
    public int StudentId { get; set; }
    public string StudentName { get; set; }
    public List<Mark> Marks { get; set; }
}
