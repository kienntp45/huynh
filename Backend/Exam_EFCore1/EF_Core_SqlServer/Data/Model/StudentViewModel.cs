﻿namespace EF_Core_SqlServer.Data.Model;

public class StudentViewModel
{
    public int StudentId { get; set; }
    public string StudentName { get; set; }
}
