﻿namespace Exam_EFCore1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarkController : ControllerBase
    {
        private readonly EFCore_Exa2Context _context;

        public MarkController(EFCore_Exa2Context context) {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetAllMarc() {
        return Ok(_context.TblMarks.ToList());   
        }

        [HttpPost]
        public IActionResult Create(TblMark mark)
        {
            _context.TblMarks.Add(mark);
            _context.SaveChanges();
            return CreatedAtAction(nameof(_context), new { id = mark.StudentId }, mark);
        }
    }
}
