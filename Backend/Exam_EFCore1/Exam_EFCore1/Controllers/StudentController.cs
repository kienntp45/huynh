﻿namespace Exam_EFCore1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IStudentServices _studentService;
        private readonly ILogger<StudentController> _logger;

        public StudentController(IStudentServices studentService, ILogger<StudentController> logger)
        {
            _studentService = studentService;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllStudents()
        {
            try
            {
                var students = await _studentService.GetALlStudent();
                return Ok(students);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi khi lấy danh sách sinh viên");
                return StatusCode(500, "Lỗi nội bộ");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateStudent([FromBody] TblStudent student)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    student.StudentId = 0; 

                    var createStudent = await _studentService.CreateStudent(student);

                    if (createStudent != null)
                    {
                        return CreatedAtAction("GetStudent", new { id = createStudent.StudentId }, createStudent);
                    }
                    return BadRequest("Không thể tạo sinh viên");
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi khi tạo sinh viên");
                return StatusCode(500, "Lỗi nội bộ");
            }
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteStudent(int id)
        {
            try
            {
                var result = await _studentService.DeleteBolean(id);
                if (result)
                {
                    return new OkObjectResult(new { Message = "Xóa thành công" });
                }
                return NotFound("Không tìm thấy bản ghi nào !");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi khi xóa sinh viên");
                return StatusCode(500, "Lỗi nội bộ"); 
            }
        }

        [HttpPut("{studentId}")]
        public async Task<IActionResult> updateStudent(int studentId, [FromBody] TblStudent student)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    student.StudentId = studentId;
                    var updateStudent = await _studentService.UpdateStudent(student);
                    if (updateStudent != null)
                    {
                        return Ok(updateStudent);
                    }
                    return NotFound($"Không tìm thấy bản ghi nào {studentId}");
                }

                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi khi cập nhật sinh viên");
                return StatusCode(500, "Lỗi nội bộ");
            }
        }

        [HttpGet("student/{studentId}")]
        public async Task<IActionResult> GetStudentWithMarks(int studentId)
        {
            try
            {
                var studentWithMarks = await _studentService.GetStudentWithMarksById(studentId);

                if (studentWithMarks == null)
                {
                    return NotFound("Không tìm thấy thông tin học sinh.");
                }

                return Ok(studentWithMarks);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi khi lấy thông tin học sinh với điểm");
                return StatusCode(500, "Lỗi nội bộ");
            }
        }


    }
}
