﻿namespace Exam_EFCore1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectController : ControllerBase
    {
        private readonly ISubjectServices _subjectServices;
        
        public SubjectController(ISubjectServices subjectServices) {
            _subjectServices = subjectServices;
        }
        [HttpGet]
        public async Task<IActionResult> GetAllSubject() // Sửa tên phương thức thành "GetAllSubject"
        {
            var subjects = await _subjectServices.GetAllSbjects(); // Sửa "wait" và "subject" thành "await" và "subjects".
            return Ok(subjects);
        }

        [HttpPost]
        public async Task<IActionResult> InsertSubject([FromBody] TblSubject subject)
        {
            if(ModelState.IsValid)
            {
                subject.SubjectId = 0;
                var insertSubject = _subjectServices.InsertSubject(subject);
                if(insertSubject!= null)
                {
                    return CreatedAtAction("GetSubject", new { id = subject.SubjectId }, insertSubject);
                }
                return BadRequest("Không thêm được");
            }
            return BadRequest(ModelState);
        }
        [HttpDelete("{Id}")]
        public async Task<IActionResult> DeleteSubject(int Id) 
        {
            var deleteSubject = await _subjectServices.DeleteSuject(Id); // Sử dụng tham số "Id" để gọi DeleteSuject
            if (deleteSubject)
            {
                return new OkObjectResult(new { Message = "Xóa thành công!" });
            }
            return NotFound("Không tìm thấy id nào");
        }
        [HttpPut("{Id}")]
        public async Task<IActionResult> UpdateSubject(int Id, [FromBody] TblSubject subject)
        {
            if (Id != subject.SubjectId)
            {
                return BadRequest("ID không khớp với dữ liệu môn học.");
            }

            var updatedSubject = await _subjectServices.UpdateSuject(subject);

            if (updatedSubject != null)
            {
                return Ok(updatedSubject);
            }

            return NotFound("Không tìm thấy môn học cần cập nhật.");
        }


        [HttpGet("subject/{subjectId}")]
        public async Task<IActionResult> GetSubjectWithMarksAndStudents(int subjectId)
        {
            var subjectWithMarksAndStudents = await _subjectServices.GetSubjectWithMarksAndStudentsById(subjectId);

            if (subjectWithMarksAndStudents == null)
            {
                return NotFound("Không tìm thấy thông tin môn học.");
            }

            return Ok(subjectWithMarksAndStudents);
        }


    }
}
