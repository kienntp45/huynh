﻿namespace Exam_EFCore1.Services
{
    public class SubjectServices : ISubjectServices
 
    {
        private readonly EFCore_Exa2Context _contextSubject;

        public SubjectServices(EFCore_Exa2Context contextDB) {
            _contextSubject = contextDB;
        }

        public async Task<bool> DeleteSuject(int SubjectId)
        {
            var DeleteSubject = await _contextSubject.TblSubjects.FindAsync(SubjectId);
            if (DeleteSubject == null)
            {
                return false;
            }

            _contextSubject.TblSubjects.Remove(DeleteSubject);
            await _contextSubject.SaveChangesAsync();

            return true;
        }


        public Task<List<TblSubject>> GetAllSbjects()
        {
           return _contextSubject.TblSubjects.ToListAsync();
          
        }

        public async Task<SubjectWithMarksAndStudents> GetSubjectWithMarksAndStudentsById(int subjectId)
        {
            var subject = await _contextSubject.TblSubjects.FindAsync(subjectId);

            if (subject == null)
            {
                return null;
            }

            var marks = await _contextSubject.TblMarks.Where(m => m.SubjectId == subjectId).ToListAsync();
            var studentIds = marks.Select(m => m.StudentId).ToList();
            var students = await _contextSubject.TblStudents.Where(s => studentIds.Contains(s.StudentId)).ToListAsync();

            var subjectWithMarksAndStudents = new SubjectWithMarksAndStudents
            {
                Subject = subject,
                Marks = marks,
                Students = students
            };

            return subjectWithMarksAndStudents;
        }

        public async Task<TblSubject> InsertSubject(TblSubject subject)
        {
            _contextSubject.TblSubjects.Add(subject);
            await _contextSubject.SaveChangesAsync();
            return subject;
        }

        public async Task<TblSubject> UpdateSuject(TblSubject subject)
        {
         var updateSubject = await _contextSubject.TblSubjects.FindAsync(subject.SubjectId); // lấy ra id của subject
            if(updateSubject == null)
            {
                return null;

            }
            updateSubject.SubjectName = subject.SubjectName;
            updateSubject.Status = subject.Status;

            _contextSubject.Entry(updateSubject).State = EntityState.Modified;
            await _contextSubject.SaveChangesAsync();

            return updateSubject;

        }
    }
}
