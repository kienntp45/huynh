﻿namespace Exam_EFCore1.Services
{
    public interface IStudentServices
    {
        Task<List<TblStudent>> GetALlStudent();
        Task<TblStudent> CreateStudent(TblStudent student);
        Task<bool> DeleteBolean(int studentId);
        Task<TblStudent> UpdateStudent(TblStudent student);
        Task<StudentWithMark> GetStudentWithMarksById(int studentId);
    }
}
