﻿namespace Exam_EFCore1.Services
{
    public interface ISubjectServices
    {
        Task<List<TblSubject>> GetAllSbjects();
        Task<TblSubject> InsertSubject(TblSubject subject);
        Task<bool> DeleteSuject(int SubjectId);
        Task<TblSubject> UpdateSuject(TblSubject subject);
        Task<SubjectWithMarksAndStudents> GetSubjectWithMarksAndStudentsById(int subjectId);

    }
}
