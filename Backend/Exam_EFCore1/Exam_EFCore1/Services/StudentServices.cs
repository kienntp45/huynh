﻿namespace Exam_EFCore1.Services
{
    public class StudentServices : IStudentServices
    {
        private readonly EFCore_Exa2Context _Context;
        private readonly ILogger<StudentServices> _logger;

        public StudentServices(EFCore_Exa2Context context, ILogger<StudentServices> logger)
        {
            _Context = context;
            _logger = logger;
        }

        public async Task<TblStudent> CreateStudent(TblStudent student)
        {
            try
            {
                _Context.TblStudents.Add(student);
                await _Context.SaveChangesAsync();
                return student;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi khi tạo Student");
                throw;
            }
        }

        public async Task<bool> DeleteBolean(int studentId)
        {
            try
            {
                var student = await _Context.TblStudents.FindAsync(studentId);
                if (student == null)
                {
                    return false;
                }
                _Context.TblStudents.Remove(student);
                await _Context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi khi xóa Student");
                throw; 
            }
        }

        public async Task<List<TblStudent>> GetALlStudent()
        {
            try
            {
                return await _Context.TblStudents.ToListAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi khi truy vấn danh sách sinh viên");
                throw; 
            }
        }

        public async Task<TblStudent> UpdateStudent(TblStudent student)
        {
            try
            {
                var updateStudent = await _Context.TblStudents.FindAsync(student.StudentId);
                if (updateStudent == null)
                {
                    return null;
                }

                // nếu tìm thấy nó sẽ cập nhập 
                updateStudent.StudentName = student.StudentName;
                updateStudent.Birthday = student.Birthday;
                updateStudent.Gender = student.Gender;
                updateStudent.Status = student.Status;

                // lưu vào cơ sở dữ liệu _context
                await _Context.SaveChangesAsync();

                return updateStudent;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi khi cập nhật sinh viên");
                throw; // Re-throw lỗi để nó có thể được xử lý ở nơi gọi.
            }
        }


        public async Task<StudentWithMark> GetStudentWithMarksById(int studentId)
        {
            try
            {
                var student = await _Context.TblStudents.FindAsync(studentId);

                if (student == null)
                {
                    return null;
                }

                var marks = await _Context.TblMarks.Where(m => m.StudentId == studentId).ToListAsync();

                var studentWithMarks = new StudentWithMark
                {
                    Student = student,
                    Marks = marks
                };

                return studentWithMarks;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Lỗi khi truy vấn sinh viên và điểm");
                throw;
            }
        }
    }
}

