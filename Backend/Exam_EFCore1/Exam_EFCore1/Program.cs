using Exam_EFCore1.models;
using Exam_EFCore1.Services;

using Exam_EFCore1.models;
using Exam_EFCore1.Services;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<EFCore_Exa2Context>(option => option.UseSqlServer
(builder.Configuration.GetConnectionString("MyStudent") ));
builder.Services.AddScoped<IStudentServices, StudentServices>();
builder.Services.AddScoped<ISubjectServices, SubjectServices>();    

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
