﻿namespace Exam_EFCore1.models
{
    public class StudentWithMark
    {
        public TblStudent Student { get; set; }
        public List<TblMark> Marks { get; set; }
    }
}
