﻿namespace Exam_EFCore1.models
{
    public partial class TblSubject
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; } = null!;
        public bool? Status { get; set; }
    }
}
