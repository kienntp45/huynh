﻿namespace Exam_EFCore1.models
{
    public partial class TblStudent
    {
        [Key] // Đánh dấu cột StudentId là primary key
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] // Cho biết cột StudentId là tự tăng
        public int StudentId { get; set; }
        public string StudentName { get; set; } = null!;
        public DateTime Birthday { get; set; }
        public int Gender { get; set; }
        public bool? Status { get; set; }
    }
}
