﻿namespace Exam_EFCore1.models
{
    public class SubjectWithMarksAndStudents
    {
        public TblSubject Subject { get; set; }
        public List<TblMark> Marks { get; set; }
        public List<TblStudent> Students { get; set; }
    }
}
