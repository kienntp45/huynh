﻿namespace Exam_EFCore1.models
{
    public partial class TblMark
    {
        public int StudentId { get; set; }
        public int SubjectId { get; set; }
        public int Scores { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual TblStudent Student { get; set; } = null!;
        public virtual TblSubject Subject { get; set; } = null!;
    }
}
