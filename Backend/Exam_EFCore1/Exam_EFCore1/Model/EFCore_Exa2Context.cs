﻿using Microsoft.EntityFrameworkCore;

namespace Exam_EFCore1.models
{
    public partial class EFCore_Exa2Context : DbContext
    {
        public EFCore_Exa2Context()
        {
        }

        public EFCore_Exa2Context(DbContextOptions<EFCore_Exa2Context> options) : base(options)
        {
        }

        public virtual DbSet<TblMark> TblMarks { get; set; } = null!;
        public virtual DbSet<TblStudent> TblStudents { get; set; } = null!;
        public virtual DbSet<TblSubject> TblSubjects { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=EFCore_Exa2;User ID=sa;Password=Kien@2810!");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblMark>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tbl_mark");

                entity.Property(e => e.CreateDate).HasColumnType("date");

                entity.HasOne(d => d.Student)
                    .WithMany()
                    .HasForeignKey(d => d.StudentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__tbl_mark__Studen__3E52440B");

                entity.HasOne(d => d.Subject)
                    .WithMany()
                    .HasForeignKey(d => d.SubjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__tbl_mark__Subjec__3F466844");
            });

            modelBuilder.Entity<TblStudent>(entity =>
            {
                entity.HasKey(e => e.StudentId)
                    .HasName("PK__tbl_stud__32C52B99E73BF94A");

                entity.ToTable("tbl_student");

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.StudentName).HasMaxLength(250);
            });

            modelBuilder.Entity<TblSubject>(entity =>
            {
                entity.HasKey(e => e.SubjectId)
                    .HasName("PK__tbl_subj__AC1BA3A8932EF11E");

                entity.ToTable("tbl_subject");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.SubjectName).HasMaxLength(200);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
