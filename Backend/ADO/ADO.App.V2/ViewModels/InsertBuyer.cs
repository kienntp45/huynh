﻿namespace ADO.App.V2.ViewModels
{
    public class InsertBuyer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PaymentMethod { get; set; }
    }
}
