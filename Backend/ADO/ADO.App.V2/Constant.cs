﻿namespace ADO.App.V2
{
    public class Constant
    {
        public const string Sp_Insert = "InsertBuyer";
        public const String Sp_show = "BEGIN :result := GetAllBuyers; END;";
        public const string Sp_Delete = "DeleteBuyer";
        public const string Sp_Update = "UpdateBuyer";
    }
}
