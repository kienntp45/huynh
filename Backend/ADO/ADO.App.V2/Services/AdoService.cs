﻿using ADO.App.V2;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Reflection.Metadata;

namespace ADO.App.Services
{
    public class AdoService : IAdoService
    {
        private readonly string connectionString;

        public AdoService(IConfiguration config)
        {
            // Đọc chuỗi kết nối từ cấu hình
            connectionString = "DATA SOURCE=localhost:1521/ORACLEXE;USER ID=system; password=Kien@123!";
        }
        public bool Insert(InsertBuyer insert)
        {
            try
            {
                using (OracleConnection connection = new OracleConnection(connectionString))
                {
                    connection.Open();

                    using (OracleCommand insertCommand = connection.CreateCommand())
                    {
                        insertCommand.CommandType = CommandType.StoredProcedure;
                        insertCommand.CommandText = V2.Constant.Sp_Insert;

                        // Thêm tham số vào thủ tục
                        insertCommand.Parameters.Add("p_Id", OracleDbType.Int32).Value = insert.Id;
                        insertCommand.Parameters.Add("p_Name", OracleDbType.NVarchar2).Value = insert.Name;
                        insertCommand.Parameters.Add("p_PaymentMethod", OracleDbType.NVarchar2).Value = insert.PaymentMethod;

                        insertCommand.ExecuteNonQuery();
                    }

                    // Sau khi chèn dữ liệu thành công, bạn có thể lấy danh sách các khách hàng (buyers) như sau
                    var buyers = ShowBuyer();

                    return true;
                }
            }
            catch (OracleException ex)
            {
                return false;
            }
        }



        public IEnumerable<InsertBuyer> ShowBuyer()
        {
            List<InsertBuyer> buyers = new List<InsertBuyer>();

            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                connection.Open();

                using (OracleCommand command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.Text; // Sử dụng CommandType.Text vì bạn đang thực hiện truy vấn SQL, không phải gọi thủ tục.
                    command.CommandText = V2.Constant.Sp_show;

                    OracleParameter resultParam = new OracleParameter("result", OracleDbType.RefCursor);
                    resultParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(resultParam);

                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            InsertBuyer buyer = new InsertBuyer
                            {
                                Id = reader.GetInt32(reader.GetOrdinal("Id")),
                                Name = reader.GetString(reader.GetOrdinal("Name")),
                                PaymentMethod = reader.GetString(reader.GetOrdinal("PaymentMethod"))
                            };

                            buyers.Add(buyer);
                        }
                    }
                }
            }

            return buyers;
        }



        




        public bool deleteBuyer(int buyerId)
        {
            try
            {
                using (OracleConnection connection = new OracleConnection(connectionString))
                {
                    connection.Open();
                    using (OracleCommand deleteCommand = connection.CreateCommand())
                    {
                        deleteCommand.CommandType = CommandType.StoredProcedure;
                        deleteCommand.CommandText = V2.Constant.Sp_Delete;

                        deleteCommand.Parameters.Add("p_Id", OracleDbType.Int32).Value = buyerId;

                        deleteCommand.ExecuteNonQuery();

                        return true;
                    }
                }
            }
            catch (OracleException ex)
            {
                // Xử lý lỗi nếu cần
                return false;
            }
        }

        public bool UpdateBuyer(InsertBuyer update)
        {
            try
            {
                using (OracleConnection connection = new OracleConnection(connectionString))
                {
                    connection.Open();

                    using (OracleCommand updateCommand = connection.CreateCommand())
                    {
                        updateCommand.CommandType = CommandType.StoredProcedure;
                        updateCommand.CommandText = V2.Constant.Sp_Update;

                        // Thêm tham số vào thủ tục
                        updateCommand.Parameters.Add("p_Id", OracleDbType.Int32).Value = update.Id;
                        updateCommand.Parameters.Add("p_NewName", OracleDbType.NVarchar2).Value = update.Name;
                        updateCommand.Parameters.Add("p_NewPaymentMethod", OracleDbType.NVarchar2).Value = update.PaymentMethod;

                        updateCommand.ExecuteNonQuery();
                    }

                    return true;
                }
            }
            catch (OracleException ex)
            {
                // Xử lý lỗi nếu cần
                return false;
            }
        }

    }
}
