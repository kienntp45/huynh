﻿using System;

namespace ADO.App.Services
{
    public interface IAdoService
    {
        bool Insert(InsertBuyer insert);

        IEnumerable<InsertBuyer> ShowBuyer();

        bool deleteBuyer(int id);

        bool UpdateBuyer(InsertBuyer update);
    }
}
