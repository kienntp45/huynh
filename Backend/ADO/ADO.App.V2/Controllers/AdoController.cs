﻿namespace ADO.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdoController : ControllerBase
    {
        private readonly IAdoService _adoService;

        public AdoController(IAdoService adoService)
        {
            _adoService = adoService;
        }


        // Thêm
        [HttpPost]
        public ActionResult Index([FromBody] InsertBuyer insert)
        {
            try
            {
                var rs = _adoService.Insert(insert);
                return Ok(rs);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message); 
            }
        }

        //Show
        [HttpGet]
        [Route("showBuyer")]
        public IActionResult GetBuyers()
        {
            try
            {
                var buyers = _adoService.ShowBuyer();
                return Ok(buyers);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        // Xóa theo ID
        [HttpDelete]
        [Route("deleteBuyer/{id}")]
        public IActionResult DeleteBuyer(int id)
        {
            try
            {
                bool delete = _adoService.deleteBuyer(id);
                if (delete)
                {
                    return Ok("Bản ghi đã được xóa");
                }
                else
                {
                    return NotFound("Không tìm thấy bản ghi ");
                }
            }catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        

        // update
        [HttpPut]
        [Route("updateBuyer")]
        public IActionResult UpdateBuyer([FromBody] InsertBuyer update)
        {
            try
            {
                var updateADO = _adoService.UpdateBuyer(update);
                if (updateADO)
                {
                    return Ok("Update thành cong!");
                }
                else
                {
                    return BadRequest("Update không thành công");
                }

            }catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
