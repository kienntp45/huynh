﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;

class Program
{
    static void Main()
    {
        string connectionString = "DATA SOURCE=localhost:1521/ORACLEXE;USER ID=system; password=Kien@123!";

        using (OracleConnection connection = new OracleConnection(connectionString))
        {
            try
            {
                connection.Open();
                Console.WriteLine("State: {0}", connection.State);
                Console.WriteLine("ConnectionString: {0}", connection.ConnectionString);




    
                ShowAllBuyer(connection);

                //ADOInsertBuyer(connection);

                //ADOUpdateBuyer(connection);

                ADODeleteBuyer(connection);

                // Chạy phương thức InserBuyer để thêm dữ liệu
                //InserBuyer(connection);

                // Chạy phương thức UpdateBuyer để cập nhật dữ liệu
                //UpdateBuyer(connection);

                // Chạy phương thức DeleteBuyer để xóa dữ liệu
                // DeleteBuyer(connection);

            }
            catch (OracleException ex)
            {
                Console.WriteLine("Lỗi Oracle: " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Lỗi: " + ex.Message);
            }
        }
    }


    //Buyer
    static void ShowAllBuyer(OracleConnection connection)
    {
        OracleCommand command = connection.CreateCommand();
        string sql = "SELECT \"Id\", \"Name\", \"PaymentMethod\" FROM \"Buyer\"";
        command.CommandText = sql;

        using (OracleDataReader reader = command.ExecuteReader())
        {
            while (reader.Read())
            {
                int id = Convert.ToInt32(reader["Id"]);
                string name = (string)reader["Name"];
                string paymentMethod = (string)reader["PaymentMethod"];

                Console.WriteLine($"ID: {id}, Name: {name}, Payment Method: {paymentMethod}");
            }
        }
    }


    static void InserBuyer(OracleConnection connection)
    {
        using (OracleCommand insertCommand = connection.CreateCommand())
        {
            Console.WriteLine("Nhap thong tin can them vao bang buyer 'Buyer':");

            Console.Write("ID: ");
            int id = int.Parse(Console.ReadLine());

            Console.Write("Tên: ");
            string name = Console.ReadLine();

            Console.Write("Phương thức thanh toán: ");
            string paymentMethod = Console.ReadLine();

            insertCommand.CommandText = "INSERT INTO \"Buyer\" (\"Id\", \"Name\", \"PaymentMethod\") " +
                                        "VALUES (:Id, :Name, :PaymentMethod)";
            insertCommand.Parameters.Add(":Id", OracleDbType.Int32).Value = id;
            insertCommand.Parameters.Add(":Name", OracleDbType.NVarchar2).Value = name;
            insertCommand.Parameters.Add(":PaymentMethod", OracleDbType.NVarchar2).Value = paymentMethod;

            int rowsInserted = insertCommand.ExecuteNonQuery();
            Console.WriteLine($"Đã thêm {rowsInserted} hàng vào bảng 'Buyer'.");
        }
    }

    static void UpdateBuyer(OracleConnection connection)
    {
        using (OracleCommand updateCommand = connection.CreateCommand())
        {
            Console.Write("Nhập ID của bản ghi cần cập nhật: ");
            int idToUpdate = int.Parse(Console.ReadLine());

            Console.Write("Tên mới: ");
            string newName = Console.ReadLine();

            Console.Write("Phương thức thanh toán mới: ");
            string newPaymentMethod = Console.ReadLine();

            updateCommand.CommandText = "UPDATE \"Buyer\" " +
                "SET \"Name\" = :NewName, \"PaymentMethod\" = :NewPaymentMethod " +
                "WHERE \"Id\" = :IdToUpdate";

            updateCommand.Parameters.Add(":NewName", OracleDbType.NVarchar2).Value = newName;
            updateCommand.Parameters.Add(":NewPaymentMethod", OracleDbType.NVarchar2).Value = newPaymentMethod;
            updateCommand.Parameters.Add(":IdToUpdate", OracleDbType.Int32).Value = idToUpdate;

            int rowsUpdated = updateCommand.ExecuteNonQuery();
            Console.WriteLine($"Đã cập nhật {rowsUpdated} hàng trong bảng 'Buyer'.");
        }
    }


    static void DeleteBuyer(OracleConnection connection)
    {
        using (OracleCommand deleteCommand = connection.CreateCommand())
        {
            Console.Write("Nhập ID của bản ghi cần xóa: ");
            int idToDelete = int.Parse(Console.ReadLine());

            deleteCommand.CommandText = "DELETE FROM \"Buyer\" WHERE \"Id\" = :IdToDelete";
            deleteCommand.Parameters.Add(":IdToDelete", OracleDbType.Int32).Value = idToDelete;

            int rowsDeleted = deleteCommand.ExecuteNonQuery();
            Console.WriteLine($"Đã xóa {rowsDeleted} hàng khỏi bảng 'Buyer'.");
        }
    }


    // ADO.net thủ tục của Table Buyer
    static void ADOInsertBuyer(OracleConnection connection)
    {
        using (OracleCommand command = new OracleCommand("InsertBuyer", connection))
        {
            command.CommandType = CommandType.StoredProcedure;

            Console.Write("Nhap ID: ");
            int id = int.Parse(Console.ReadLine());

            Console.Write("Nhap Ten: ");
            string name = Console.ReadLine();

            Console.Write("Nhap phuong thuc thanh toan: ");
            string paymentMethod = Console.ReadLine();

            OracleParameter paramId = new OracleParameter("p_Id", OracleDbType.Int32);
            paramId.Value = id;
            command.Parameters.Add(paramId);

            OracleParameter paramName = new OracleParameter("p_Name", OracleDbType.NVarchar2, 50);
            paramName.Value = name;
            command.Parameters.Add(paramName);

            OracleParameter paramPaymentMethod = new OracleParameter("p_PaymentMethod", OracleDbType.NVarchar2, 50);
            paramPaymentMethod.Value = paymentMethod;
            command.Parameters.Add(paramPaymentMethod);

            command.ExecuteNonQuery();
        }
    }

    static void ADOUpdateBuyer(OracleConnection connection)
    {
        using (OracleCommand updateCommand = connection.CreateCommand())
        {
            Console.Write("Nhap ID cua ban ghi can cap nhap: ");
            int id = int.Parse(Console.ReadLine());

            Console.Write("Nhap ten mơi: ");
            string newName = Console.ReadLine();

            Console.Write("Nhap phuong thuc thanh toan: ");
            string newPaymentMethod = Console.ReadLine();

            updateCommand.CommandText = "UPDATE \"Buyer\" " +
                "SET \"Name\" = :NewName, \"PaymentMethod\" = :NewPaymentMethod " +
                "WHERE \"Id\" = :IdToUpdate";

            updateCommand.Parameters.Add(":NewName", OracleDbType.NVarchar2).Value = newName;
            updateCommand.Parameters.Add(":NewPaymentMethod", OracleDbType.NVarchar2).Value = newPaymentMethod;
            updateCommand.Parameters.Add(":IdToUpdate", OracleDbType.Int32).Value = id;

            int rowsUpdated = updateCommand.ExecuteNonQuery();
            Console.WriteLine($"Đã cập nhật {rowsUpdated} hàng trong bảng 'Buyer'.");
        }
    }

    static void ADODeleteBuyer(OracleConnection connection)
    {
        using (OracleCommand deleteCommand = connection.CreateCommand())
        {
            Console.Write("Nhap ID can xoa: ");
            int id = int.Parse(Console.ReadLine());

            deleteCommand.CommandText = "DELETE FROM \"Buyer\" WHERE \"Id\" = :IdToDelete";
            deleteCommand.Parameters.Add(":IdToDelete", OracleDbType.Int32).Value = id;

            int rowsDeleted = deleteCommand.ExecuteNonQuery();
            Console.WriteLine($"Đã xóa {rowsDeleted} hàng khỏi bảng 'Buyer'.");
        }
    }





}
