class Buyer
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string PaymentMethod { get; set; }
}
