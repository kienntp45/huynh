﻿namespace ADO.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdoController : ControllerBase
    {
        private readonly IAdoService _adoService;

        public AdoController(IAdoService adoService)
        {
            _adoService = adoService;
        }
    }
}
