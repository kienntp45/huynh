﻿using demoAPI.Model;
using Microsoft.AspNetCore.Mvc;


[Route("api/[controller]")]
[ApiController]
public class DemoController : ControllerBase
{
    private IConfiguration _configuration; // Khai báo biến _configuration để sử dụng

    public DemoController(IConfiguration configuration)
    {
        _configuration = configuration; // Khởi tạo _configuration
    }

    [HttpGet]
    public IActionResult Get()
    {
        var people = _configuration.GetSection("DataJson").Get<List<MyData>>();

        var value3 = _configuration.GetSection("Key1:Key2:Key3").Value;

        var valueObject = _configuration.GetSection("TenKhac:Key5:Key6").Get<List<MyData>>();
        return Ok(valueObject);
    }
}
