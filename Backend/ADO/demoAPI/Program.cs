﻿using System;
using System.Data.SqlClient;

class Program
{
    static void Main()
    {
        // Chuỗi kết nối cơ sở dữ liệu SQL Server
        string connectionString = "Data Source=localhost,1521;Initial Catalog=testDB;User ID=system;Password=Kien@123!";

        // Tạo đối tượng SqlConnection
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            try
            {
                // Mở kết nối
                connection.Open();
                Console.WriteLine("Kết nối cơ sở dữ liệu thành công.");

                // Thực hiện các thao tác với cơ sở dữ liệu ở đây

                // Đóng kết nối
                connection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Lỗi khi kết nối cơ sở dữ liệu: " + ex.Message);
            }
        }
    }
}
