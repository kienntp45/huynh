﻿namespace demoAPI.Model
{
    public class ConnectionSettings
    {
        public string DataSource { get; set; }
        public string InitialCatalog { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
    }
}
