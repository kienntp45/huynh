﻿namespace demoAPI.Model
{
    public class MyData
    {
        public string Name { get; set; }
        public int Tuoi { get; set; }
        public string Email { get; set; }
    }
}
